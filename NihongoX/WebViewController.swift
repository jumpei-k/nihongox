//
//  WebViewController.swift
//  LearnJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/30/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

private let baseImageURL = "https://www.google.co.jp/search?tbm=isch&q="
private let baseWikiURL = "https://ja.wikipedia.org/wiki/"
private let siteTypes = ["wiki", "googleImage"]

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var fullURL: String!
    var keyword: String?
    
    var siteTypeId: Int!
    
    var siteName: String {
        get { return siteTypes[siteTypeId] }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch siteName {
        case "wiki":
            fullURL = baseWikiURL + keyword!
        case "googleImage":
            fullURL = baseImageURL + keyword!
        default: break;
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let reqUrl = NSURL(string: self.fullURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        let request = NSURLRequest(URL: reqUrl!)
        webView?.loadRequest(request)

    }
    
    @IBAction func dismiss(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
