//
//  PhraseTableViewCell.swift
//  LearningJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/11/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

// Cell calss for PhraseListViewController

import UIKit

class PhraseTableViewCell: UITableViewCell {

    @IBOutlet weak var labelEnglish: UILabel!
    @IBOutlet weak var labelJapanese: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
