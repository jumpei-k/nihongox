//
//  NXUserContentVocabularyCell.swift
//  NihongoX
//
//  Created by Jumpei Katayama on 10/12/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class NXUserContentVocabularyCell: UITableViewCell {
    
    @IBOutlet weak var labelEnglish: UILabel!
    @IBOutlet weak var labelJapanese: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
