//
//  SearchMenuViewController.swift
//  LearnJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/21/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
//import DrawerController
import RealmSwift

private let identifier = "search cell"

struct SearchCellIdentifier {
    static let kanji = "search kanji"
    static let vocabulary = "search vocabulary"
    static let phrase = "search phrase"
}

struct SearchSegueIdentifier {
    static let fromKanji = "from kanji search"
    static let fromVocabulary = "from vocabulary search"
    static let fromPhrase = "from phrase search"
}

class PhraseSearchCell: UITableViewCell {
    
    @IBOutlet weak var englishLabel: UILabel!
}

class KanjiSearchCell: UITableViewCell {
    @IBOutlet weak var englishLabel: UILabel!
}

class VocabularySearchCell: UITableViewCell {
    @IBOutlet weak var englishLabel: UILabel!
}


class SearchMenuViewController: NXCenterContentsBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    // Search results array
    // Every time search text changes, a new result array is created
    // The search array
    var searchResults: [ResultsBase]!
    var searchOnline: [String]!
    var phraseResultsSet: Results<Phrase>!
    var kanjiResultsSet: Results<Kanji>!
    var vocaResultsSet: Results<Vocabulary>!
    var searchController: UISearchController!
    
    var searchText: String? {
        didSet {
            if searchText == "" {
                // show default tableView
                // It should show no result
                print("no input")
            } else {
                // Here search result table view comes
                do {
                    let realm = try Realm()
                    phraseResultsSet = realm.objects(Phrase).filter("english contains %@", searchText!)
                    vocaResultsSet = realm.objects(Vocabulary).filter("english contains %@", searchText!)
                    kanjiResultsSet = realm.objects(Kanji).filter("meaning contains %@", searchText!)
                    // If no results
                    guard phraseResultsSet.count != 0 || vocaResultsSet.count != 0 || kanjiResultsSet.count != 0 else {
                        print("no resutls")
                        return
                    }

                    print("\(phraseResultsSet.count)")
                    print("\(vocaResultsSet.count)")
                    print("\(kanjiResultsSet.count)")
                    searchResults = [phraseResultsSet, vocaResultsSet, kanjiResultsSet]
                    print("didFetched")
                } catch {
                    print(error)
                }
                print("there are search resutls")
            }
        }
    }
    
    // ----------------------
    // MARK: - View LifeCycle
    // ----------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchController()
        print("Search menu")
        searchResults = []
        // Prevent keyboard from showing up when side menu opnes
//        self.evo_drawerController?.drawerVisualStateBlock = ({ (drawerController: DrawerController, drawerSide: DrawerSide, percentVisible: CGFloat) in
//            if drawerSide == .Left {
//                self.searchController.searchBar.resignFirstResponder()
//            }
//        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        searchController.searchBar.becomeFirstResponder()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }
    
    override func leftDrawerButtonPress(sender: AnyObject) {
        super.leftDrawerButtonPress(sender)
    }
    
    // Pass the data object and index section so the destination can handle which data type: Kanji, Phrase, and Vocabulary to cast
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dest = segue.destinationViewController as? SearchDetailViewController {
            if let identifier = segue.identifier {
                switch identifier {
                    case SearchSegueIdentifier.fromPhrase:
                        if let cell = sender as? UITableViewCell {
                            dest.sectionNum = tableView.indexPathForCell(cell)?.section
                            dest.searchedObject = phraseResultsSet[(tableView.indexPathForCell(cell)?.row)!]}
                    case SearchSegueIdentifier.fromVocabulary:
                        if let cell = sender as? UITableViewCell {
                            dest.sectionNum = tableView.indexPathForCell(cell)?.section
                            dest.searchedObject = vocaResultsSet[(tableView.indexPathForCell(cell)?.row)!]
                    }
                    case SearchSegueIdentifier.fromKanji:
                        if let cell = sender as? UITableViewCell {
                            dest.sectionNum = tableView.indexPathForCell(cell)?.section
                            dest.searchedObject = kanjiResultsSet[(tableView.indexPathForCell(cell)?.row)!]
                    }
                    default: break;
                }
            }
        }
    }
    
    // ----------------
    //  MARK: - Private
    // ----------------
    
    private func setupSearchController() {
        // ssetup search controller
        searchController = UISearchController(searchResultsController: nil)
        navigationItem.titleView = searchController.searchBar
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        searchController.definesPresentationContext = true
    }
}

extension SearchMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 88
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return searchResults.count
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfRows: Int!
        switch section {
        case 0:
            numOfRows = phraseResultsSet.count
        case 1:
            numOfRows = vocaResultsSet.count
        case 2:
            numOfRows = kanjiResultsSet.count
        default: break
        }
        return numOfRows
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        // Depending on the section, the tableViewCell changes
        switch indexPath.section {
        case 0:// Configure kanji cell
            let phraseCell = tableView.dequeueReusableCellWithIdentifier(SearchCellIdentifier.phrase, forIndexPath: indexPath) as! PhraseSearchCell
            phraseCell.englishLabel.text = phraseResultsSet[indexPath.row].english
            cell = phraseCell

        case 1:// Configure voca cell
            let vocaCell = tableView.dequeueReusableCellWithIdentifier(SearchCellIdentifier.vocabulary, forIndexPath: indexPath) as! VocabularySearchCell
            vocaCell.englishLabel.text = vocaResultsSet[indexPath.row].english
            cell = vocaCell

        case 2:// Configure phrase cell
            let kanjiCell = tableView.dequeueReusableCellWithIdentifier(SearchCellIdentifier.kanji, forIndexPath: indexPath) as! KanjiSearchCell
            kanjiCell.englishLabel.text = kanjiResultsSet[indexPath.row].meaning
            cell = kanjiCell
            
        default: break;
            
        }

        return cell
    }
}

extension SearchMenuViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        print("update")
        let sb = searchController.searchBar
        searchText = sb.text
        tableView.reloadData()
    }
}

extension SearchMenuViewController: UISearchBarDelegate {
    
}
