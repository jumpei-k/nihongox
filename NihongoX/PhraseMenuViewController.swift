//
//  PhraseMenuViewController.swift
//  LearningJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/8/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

private let objectives = ["Greeting", "Thanks and Apologies", "When you leave", "When you are lost", "When you meet some one for the first time", "Annual Events", "No Idea", "Ask good questions", "Time and Place", "Personality Descriptions", "Study", "Wish", "Shopping"]

private let identifier = "objective cell"

class PhraseMenuViewController: NXCenterContentsBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dest = segue.destinationViewController as? PhraseListViewController {
            if let cell = sender as? UITableViewCell {
                dest.query = (tableView.indexPathForCell(cell)?.row)! + 1
            }
        }
    }
}

extension PhraseMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectives.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
        cell.textLabel?.text = objectives[indexPath.row]
        
        return cell
    }
    
    
}
