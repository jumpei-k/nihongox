//
//  NXRootViewController.swift
//  LearnJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 10/4/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import RESideMenu

class NXRootViewController: RESideMenu, RESideMenuDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func awakeFromNib() {
        
        self.menuPreferredStatusBarStyle = UIStatusBarStyle.LightContent
        self.contentViewShadowColor = UIColor.blackColor();
        self.contentViewShadowOffset = CGSizeMake(0, 0);
        self.contentViewShadowOpacity = 0.6;
        self.contentViewShadowRadius = 12;
        self.contentViewShadowEnabled = true;
        
        self.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PhraseMenuNavigation")
        self.leftMenuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LeftMenu")
        
    }
    
    // MARK: RESide Delegate Methods
    
    func sideMenu(sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        print("This will show the menu")
    }



}
