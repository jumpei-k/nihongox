//
//  UserContentsBaseViewController.swift
//  LearningJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/11/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

/**
*    Base class for user contents view controller: DeckMenuViewController and FavoriteMenuController.
*
*/

import UIKit
import RealmSwift

private let userContentsChoice = ["isFavorite", "inFolder"]
private let navigationTitles = ["Favorite", "Folder"]

struct CellIdentifier {
//    static let kanji = "kanji cell"
    static let vocabulary = "user vocabulary cell"
    static let phrase = "user phrase cell"
}

struct SegueIdentifier {
//    static let toKanjiPage = "to kanji page container"
    static let toVocaPage = "to voca page container"
    static let toPhrasePage = "to phrase page container"
}

class UserContentsBaseViewController: NXCenterContentsBaseViewController {
    
//    var kanjiSet: Results<Kanji>!
    var phraseSet: Results<Phrase>!
    var vocaSet: Results<Vocabulary>!
    var realm: Realm!
    
    var choiceId: Int!
    var segmentIndex: Int = 0
    var userContentsString: String!
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        userContentsString = userContentsChoice[choiceId]
        super.viewDidLoad()
        do {
            realm = try Realm()
        } catch {
            print(error)
        }
        navigationItem.title = navigationTitles[choiceId]
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reload(segmentIndex, userChoice: choiceId)

    }

    // The user can choose phrase or vocabulary based on the segment index
    @IBAction func changeSegmentIndex(sender: AnyObject) {
        if let segment = sender as? UISegmentedControl {
            segmentIndex = segment.selectedSegmentIndex
            reload(segmentIndex, userChoice: choiceId)
            print(segment.selectedSegmentIndex)
        }
    }
    
    /**
    Reload tableData with segmentID and userchoice
    
    :param: segmentIndex 0: Kanji, 1: Vocabulary, 2: Phrase
    :param: userChoice   0: isFavorite, 1: inDeck for userContentsChoice
    */
    func reload(segmentIndex: Int, userChoice: Int) {
        print("reloading..")

        let userChoiceStr: String = userContentsChoice[choiceId]
        var predicate: NSPredicate!
        
        if userChoiceStr == "isFavorite" {
            predicate = NSPredicate(format: "isFavorite == 1")
        } else {
            predicate = NSPredicate(format: "inDeck == 1")
        }
        print(userChoiceStr)
        switch segmentIndex {
        case 0:// Kanji
            print("")
            vocaSet = realm.objects(Vocabulary).filter(predicate)

//            kanjiSet = realm.objects(Kanji).filter(predicate)
        case 1:// Vocabulary
            phraseSet = realm.objects(Phrase).filter(predicate)

//        case 2:// Phrase
        default: break;
        }

        tableView.reloadData()
        print("reloaded")
    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch identifier {
//            case SegueIdentifier.toKanjiPage:
//                if let dest = segue.destinationViewController as? PageContainerViewController {
//                    if let cell = sender as? UITableViewCell {
//                        dest.kanjiSet = self.kanjiSet
//                        dest.pageIndex = tableView.indexPathForCell(cell)?.row
//                    }
//                }
            case SegueIdentifier.toVocaPage:
                if let dest = segue.destinationViewController as? VocabularyPageContainerController {
                    if let cell = sender as? UITableViewCell {
                        dest.dataSet = self.vocaSet
                        dest.pageIndex = tableView.indexPathForCell(cell)?.row
                    }
                }
            case SegueIdentifier.toPhrasePage:
                if let dest = segue.destinationViewController as? PhrasePageContainerController {
                    if let cell = sender as? UITableViewCell {
                        dest.dataSet = self.phraseSet
                        dest.pageIndex = tableView.indexPathForCell(cell)?.row
                    }
                }
                print("")
            default: break;
                
            }
        }
    }
}


extension UserContentsBaseViewController: UITableViewDataSource, UITableViewDelegate {
    
    // Set data depending on the segment
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch segmentIndex {
        case 0:
//            rows = kanjiSet.count
            print("")
        case 1:
            rows = vocaSet.count
        case 2:
            rows = phraseSet.count
        default: break;
        }
        return rows
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        switch segmentIndex {
//        case 0:
//            let kanjiCell = self.tableView.dequeueReusableCellWithIdentifier(CellIdentifier.kanji, forIndexPath: indexPath) as! KanjiTableViewCell
//            kanjiCell.englishLabel.text = kanjiSet[indexPath.row].meaning
//            kanjiCell.japaneseLabel.text = kanjiSet[indexPath.row].name
//            cell = kanjiCell
        case 0:
            let vocaCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.vocabulary, forIndexPath: indexPath) as! NXUserContentVocabularyCell
            vocaCell.labelEnglish.text = vocaSet[indexPath.row].english
            vocaCell.labelJapanese.text = vocaSet[indexPath.row].japanese
            
            cell = vocaCell
            
        case 1:
            let phraseCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier.phrase, forIndexPath: indexPath) as! NXUserContentPhraseCell
            phraseCell.labelEnglish.text = phraseSet[indexPath.row].english
            phraseCell.labelJapanese.text = phraseSet[indexPath.row].japaneseFormal
            cell = phraseCell
        default: break;
        }
        return cell
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 88
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch segmentIndex {
        case 0:
            switch editingStyle {
            case .Delete:
//                let kanji = self.kanjiSet[indexPath.row]
                do {
                    try realm.write({
                        if self.userContentsString == "isFavorite" {
//                            kanji.isFavorite = false
                        } else {
//                            kanji.inDeck = false
                        }
                    })
                } catch {
                    print(error)
                }
                
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)

            default: break;
            }
        case 1:
            switch editingStyle {
            case .Delete:
                let voca = self.vocaSet[indexPath.row]
                do {
                    try realm.write({
                        if self.userContentsString == "isFavorite" {
                            voca.isFavorite = false
                        } else {
                            voca.inFolder = false
                        }
                        
                    })
                    
                } catch {
                    print(error)
                }
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            default: break;
            }
        case 2:
            switch editingStyle {
            case .Delete:
                let phrase = self.phraseSet[indexPath.row]
                do {
                    try realm.write({
                        if self.userContentsString == "isFavorite" {
                            phrase.isFavorite = false
                        } else {
                            phrase.inFolder = false
                        }
                    })
                } catch {
                    print(error)
                }
              
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            default: break;
            }
        default: break;
        }

        tableView.reloadData()
        print("successfully deleted")

    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

}
