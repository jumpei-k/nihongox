//
//  LeftSideMenuViewController.swift
//  LearningJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/8/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import RESideMenu
//import DrawerController


private let menus = ["Search", "Favorite", "Folder", "Vocabulary", "Phrase"]
private let identifier = "menu cell"

struct CenterMenu {
    static let search = "SearchMenuNavigation"
    static let userCustom = "UserMenuNavigation"
    static let favorite =  "FavoriteMenuNavigation"
    static let folder = "FolderMenuNavigation"
//    static let kanji = "KanjiMenuNavigation"
    static let phrase = "PhraseMenuNavigation"
    static let vocabulary = "VocabularyMenuNavigation"
}


enum Menu: Int {
    case Search = 0
    case Favorite
    case Folder
//    case Kanji
    case Vocabulary
    case Phrase
}


class LeftSideMenuTableViewController: MenuBaseViewController {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title
//        navigationItem.title = "LearnJapaneseTheHardWay"
        // Hide empty cells
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.alwaysBounceVertical = false
    }
}

extension LeftSideMenuTableViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)
        
        if let menu = Menu(rawValue: indexPath.row) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            switch menu {
            case .Search:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                sideMenuViewController.setContentViewController(storyboard.instantiateViewControllerWithIdentifier(CenterMenu.search) as! UINavigationController, animated: true)
                sideMenuViewController.hideMenuViewController()
//                let userVc = centerVc.topViewController as! SearchMenuViewController
//                userVc.choiceId = 0
            case .Favorite:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                sideMenuViewController.setContentViewController(storyboard.instantiateViewControllerWithIdentifier(CenterMenu.userCustom) as! UINavigationController, animated: true)
                sideMenuViewController.hideMenuViewController()

//                let centerVc = storyboard.instantiateViewControllerWithIdentifier(CenterMenu.userCustom) as! UINavigationController
//                let userVc = centerVc.topViewController as! UserContentsBaseViewController
//                userVc.choiceId = 0
            case .Folder:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                sideMenuViewController.setContentViewController(storyboard.instantiateViewControllerWithIdentifier(CenterMenu.userCustom) as! UINavigationController, animated: true)
                sideMenuViewController.hideMenuViewController()

//                let centerVc = storyboard.instantiateViewControllerWithIdentifier(CenterMenu.userCustom) as! UINavigationController
//                let userVc = centerVc.topViewController as! UserContentsBaseViewController
//                userVc.choiceId = 1

            case .Vocabulary:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                sideMenuViewController.hideMenuViewController()

                sideMenuViewController.setContentViewController(storyboard.instantiateViewControllerWithIdentifier(CenterMenu.vocabulary) as! UINavigationController, animated: true)

//                let centerVc = storyboard.instantiateViewControllerWithIdentifier(CenterMenu.vocabulary)
//                evo_drawerController?.setCenterViewController(centerVc, withCloseAnimation: true, completion: nil)
            case .Phrase:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                sideMenuViewController.setContentViewController(storyboard.instantiateViewControllerWithIdentifier(CenterMenu.phrase) as! UINavigationController, animated: true)
                sideMenuViewController.hideMenuViewController()

//                let centerVc = storyboard.instantiateViewControllerWithIdentifier(CenterMenu.phrase)
//                evo_drawerController?.setCenterViewController(centerVc, withCloseAnimation: true, completion: nil)
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
        cell.textLabel?.text = menus[indexPath.row]
        
        return cell
    }
    
    
}
