//
//  VocabularyMenuViewController.swift
//  LearningJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/9/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

private let identifier = "vocabulary category"
private let categories = ["food","verb","basic"]


class VocabularyCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var labelCategory: UILabel!
}


class VocabularyMenuViewController: NXCenterContentsBaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destination = segue.destinationViewController as? VocabularyListViewController {
            if let button = sender as? UIButton {
                let categoryId = button.tag
                destination.categoryId = categoryId
            }
        }
    }
}

extension VocabularyMenuViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath) as! VocabularyCategoryCell
        cell.tag = indexPath.row + 1
        cell.labelCategory.text = categories[indexPath.row]
        return cell
    }
}

