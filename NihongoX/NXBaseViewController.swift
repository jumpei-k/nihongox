//
//  CenterMenuBaseViewController.swift
//  LearningJapaneseTheHardWay
//
//  Created by Jumpei Katayama on 8/10/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
//import DrawerController


class NXBaseViewController: UIViewController {

    var viewCtlName: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupLeftMenuButton()
    }
 
    func setupLeftMenuButton() {

    }
    
    //  MARK: - Button Handlers:
    func leftDrawerButtonPress(sender: AnyObject) {
//        self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("Entering \(viewCtlName)...")
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        print("Exsiting \(viewCtlName)...")
    }
    
    
    
}
